(function ($) {

Drupal.behaviors.date_time_extras = function (context) {
  // Hide start/end labels.
  $('.date-time-extras-date > .container-inline-date > .form-item > label').html('');

  // Initialize 'All Day' checkboxes.
  $('.date-time-extras-combo-option .date-time-extras-all-day:not(.date-time-extras-processed)').change(function () {
    var elements = null;
    if ($(this).is('.date-time-extras-all-day-date-popup')) {
      // Toggle time entry.
      elements = $(this).closest('.date-time-extras-combo-option').siblings('.date-time-extras-date').find(':text[name$="[time]"][id*="-timeEntry-"]').closest('.form-item');
    }
    else if ($(this).is('.date-time-extras-all-day-date-select')) {
      // Toggle sub-day values.
      elements = $(this).closest('.date-time-extras-combo-option').siblings('.date-time-extras-date').find('.date-hour, .date-minute, .date-second');
    }

    if (elements !== null) {
      if ($(this).is(':checked')) {
        elements.hide();
      }
      else {
        elements.show();
      }
    }
  }).addClass('date-time-extras-processed').trigger('change');

  // Initialize 'Show End Date' checkboxes.
  $('.date-time-extras-combo-option .date-time-extras-show-end-date:not(.date-time-extras-processed)').change(function () {
    var endDate = $(this).closest('.date-time-extras-combo-option').siblings('.date-time-extras-date-end');
    if ($(this).is(':checked')) {
      endDate.show();
    }
    else {
      endDate.hide();
    }
  }).addClass('date-time-extras-processed').trigger('change');
};

})(jQuery);