<?php

/**
 * Declares widget types to extend.
 */
function hook_date_time_extras_widgets() {
  return array(
    'date_select' => array(
      'partial' => TRUE,
    ),
    'date_popup' => array(
      'partial' => TRUE,
    ),
  );
}
